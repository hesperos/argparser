#ifndef _COMMON_HPP_
#define _COMMON_HPP_

#define WRAP(x) #x
#define STRINGIFY(x) WRAP(x)

#endif // !_COMMON_HPP_
