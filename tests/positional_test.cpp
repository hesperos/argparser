#include <gtest/gtest.h>

#include "common.hpp"

#include <argparser/exceptions.hpp>
#include <argparser/positional.hpp>

using namespace argparser;

class PositionalTest : public ::testing::Test {
public:
};

TEST_F(PositionalTest, test_ifNameMatches) {
    Positional p("X");
    ASSERT_EQ("X", p.getName());
}

TEST_F(PositionalTest, test_ifIsMatchingReturnsFalse) {
    Positional p("X");
    ASSERT_FALSE(p.isMatching("X"));
    ASSERT_FALSE(p.isMatching(""));
    ASSERT_FALSE(p.isMatching("abc"));
}

TEST_F(PositionalTest, test_ifCumulativeWorksWithStringTypes) {
    Positional p("X");
    std::vector<std::string> v;

    ASSERT_FALSE(p.isCumulative());

    p.set(v);
    ASSERT_TRUE(p.isCumulative());

    p.setValue("first");
    ASSERT_EQ(1, v.size());
    ASSERT_EQ("first", v.at(0));

    p.setValue("second");
    ASSERT_EQ(2, v.size());
    ASSERT_EQ("first", v.at(0));
    ASSERT_EQ("second", v.at(1));

    p.setValue("third");
    ASSERT_EQ(3, v.size());
    ASSERT_EQ("first", v.at(0));
    ASSERT_EQ("second", v.at(1));
    ASSERT_EQ("third", v.at(2));

    ASSERT_EQ("first,second,third", p.getValue());
}

TEST_F(PositionalTest, test_ifCumulativeWorksWithIntegerTypes) {
    Positional p("X");
    std::vector<int> v;

    ASSERT_FALSE(p.isCumulative());

    p.set(v);
    ASSERT_TRUE(p.isCumulative());

    p.setValue("123");
    ASSERT_EQ(1, v.size());
    ASSERT_EQ(123, v.at(0));

    p.setValue("456");
    ASSERT_EQ(2, v.size());
    ASSERT_EQ(123, v.at(0));
    ASSERT_EQ(456, v.at(1));

    p.setValue("789");
    ASSERT_EQ(3, v.size());
    ASSERT_EQ(123, v.at(0));
    ASSERT_EQ(456, v.at(1));
    ASSERT_EQ(789, v.at(2));
}

TEST_F(PositionalTest, test_ifCumulativeWorksWithBooleanTypes) {
    Positional p("X");
    std::vector<bool> v;

    ASSERT_FALSE(p.isCumulative());

    p.set(v);
    ASSERT_TRUE(p.isCumulative());

    p.setValue("true");
    ASSERT_EQ(1, v.size());
    ASSERT_EQ(true, v.at(0));

    p.setValue("false");
    ASSERT_EQ(2, v.size());
    ASSERT_EQ(true, v.at(0));
    ASSERT_EQ(false, v.at(1));

    p.setValue("FALSE");
    ASSERT_EQ(3, v.size());
    ASSERT_EQ(true, v.at(0));
    ASSERT_EQ(false, v.at(1));
    ASSERT_EQ(false, v.at(2));

    p.setValue("TRUE");
    ASSERT_EQ(4, v.size());
    ASSERT_EQ(true, v.at(0));
    ASSERT_EQ(false, v.at(1));
    ASSERT_EQ(false, v.at(2));
    ASSERT_EQ(true, v.at(3));

    p.setValue("0");
    ASSERT_EQ(5, v.size());
    ASSERT_EQ(true, v.at(0));
    ASSERT_EQ(false, v.at(1));
    ASSERT_EQ(false, v.at(2));
    ASSERT_EQ(true, v.at(3));
    ASSERT_EQ(false, v.at(4));

    p.setValue("1");
    ASSERT_EQ(6, v.size());
    ASSERT_EQ(true, v.at(0));
    ASSERT_EQ(false, v.at(1));
    ASSERT_EQ(false, v.at(2));
    ASSERT_EQ(true, v.at(3));
    ASSERT_EQ(false, v.at(4));
    ASSERT_EQ(true, v.at(5));
}

TEST_F(PositionalTest, test_ifThrowsIfPositionalsNameIsInvalid) {
    ASSERT_THROW(Positional(""), ArgDeclEx);
    ASSERT_THROW(Positional("--start-with-a-double-hyphen"), ArgDeclEx);
    ASSERT_THROW(Positional("-start-with-a-single-hyphen"), ArgDeclEx);
    ASSERT_THROW(Positional("contains spaces"), ArgDeclEx);
}

TEST_F(PositionalTest, test_ifOpensStreamForReading) {
    Positional p("X");
    std::ifstream ifs;

    p.set(ifs);
    ASSERT_FALSE(p.isCumulative());
    ASSERT_FALSE(ifs.is_open());

    const std::string pv = STRINGIFY(TEST_ROOT) "/testdata/testfile.txt";

    p.setValue(pv);
    ASSERT_TRUE(ifs.is_open()) << "path: " << pv;
}

TEST_F(PositionalTest, test_ifOpensStreamForWriting) {
    Positional p("X");
    std::ofstream ofs;

    p.set(ofs);
    ASSERT_FALSE(p.isCumulative());
    ASSERT_FALSE(ofs.is_open());

    const auto pv = std::filesystem::temp_directory_path() / "myfile.txt";

    p.setValue(pv.string());
    EXPECT_TRUE(ofs.is_open()) << "path: " << pv;
    EXPECT_TRUE(p.isAssigned());
    EXPECT_EQ(pv.string(), p.getValue());

    if (std::filesystem::exists(pv)) {
        std::filesystem::remove(pv);
    }
}

TEST_F(PositionalTest, test_ifThrowsOnOpenFailure) {
    Positional p("X");
    std::ofstream ofs;

    p.set(ofs);
    ASSERT_FALSE(p.isCumulative());
    ASSERT_FALSE(ofs.is_open());

    ASSERT_THROW(p.setValue("/this/path/for/sure/doesnt/exist"),
                 ArgConversionEx);
}
