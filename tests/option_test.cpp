#include <gtest/gtest.h>

#include <argparser/exceptions.hpp>
#include <argparser/option.hpp>

using namespace argparser;

class OptionTest : public ::testing::Test {
public:
};

TEST_F(OptionTest, test_ifIsNullaryIsFalse) {
    Option o(shortName = "o", longName = "option");
    ASSERT_FALSE(o.isNullary());
}

TEST_F(OptionTest, test_ifShortAndLongNameGenerationWorks) {
    Option sl(shortName = "o", longName = "option");
    Option sOnly(shortName = "o");
    Option lOnly(longName = "option");

    ASSERT_EQ("-o|--option", sl.getName());
    ASSERT_EQ("-o", sOnly.getName());
    ASSERT_EQ("--option", lOnly.getName());
}

TEST_F(OptionTest, test_ifShortAndLongMatchingWorks) {
    Option o(shortName = "o", longName = "option");

    ASSERT_TRUE(o.isMatching("-o"));
    ASSERT_TRUE(o.isMatching("--option"));

    ASSERT_FALSE(o.isMatching("-oat"));
    ASSERT_FALSE(o.isMatching("--other"));
    ASSERT_FALSE(o.isMatching("-a"));
    ASSERT_FALSE(o.isMatching("-x"));
}

TEST_F(OptionTest, test_ifShortAndLongMatchingWorks2) {
    Option sOnly(shortName = "o");
    Option lOnly(longName = "option");

    ASSERT_TRUE(sOnly.isMatching("-o"));
    ASSERT_FALSE(lOnly.isMatching("-o"));
    ASSERT_FALSE(sOnly.isMatching("--option"));
    ASSERT_TRUE(lOnly.isMatching("--option"));
}

TEST_F(OptionTest, test_ifAssigningToIntegersWorks) {
    Option o(longName = "option");
    int i1 = 0;

    ASSERT_EQ(0, i1);
    o.set(i1);

    o.setValue("123");
    ASSERT_EQ(123, i1);

    o.setValue("-10");
    ASSERT_EQ(-10, i1);
}

TEST_F(OptionTest, test_ifOverflowIsDetected) {
    Option o(shortName = "o");
    uint8_t u1 = 0;

    ASSERT_EQ(0, u1);
    o.set(u1);

    ASSERT_THROW(o.setValue("256"), ArgConversionEx);
    ASSERT_THROW(o.setValue("-1"), ArgConversionEx);
}

TEST_F(OptionTest, test_ifThrowsIfShortLongOptionNameVariantsAreInvalid) {
    ASSERT_THROW(Option(shortName = "has spaces"), ArgDeclEx);
    ASSERT_THROW(Option(longName = "has spaces"), ArgDeclEx);

    ASSERT_THROW(Option(shortName = ""), ArgDeclEx);
    ASSERT_THROW(Option(longName = ""), ArgDeclEx);

    ASSERT_THROW(Option(shortName = "-has-hyphen"), ArgDeclEx);
    ASSERT_THROW(Option(longName = "--has-double-hyphen"), ArgDeclEx);
}
