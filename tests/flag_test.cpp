#include <gtest/gtest.h>

#include <argparser/flag.hpp>

using namespace argparser;

class FlagTest : public ::testing::Test {
public:
};

TEST_F(FlagTest, test_ifIsNullaryReturnsTrue) {
    Flag f{shortName = "f", longName = "flag"};
    ASSERT_TRUE(f.isNullary());
}

TEST_F(FlagTest, test_ifFlagAssignmentWorks) {
    Flag f{shortName = "f", longName = "flag"};
    bool flag = false;

    f.set(flag);
    f.setValue("");
    ASSERT_TRUE(flag);
}
