#include <gtest/gtest.h>

#include <argparser/func_traits.hpp>

#include <string>
#include <type_traits>

namespace {

[[maybe_unused]] int foo(float, std::string, int) {
    return 123;
}

} // namespace

using namespace argparser::priv;

TEST(FuncTraits, test_ifTypesAreAsExpectedForFuncPointer) {
    ASSERT_TRUE((std::is_same<float, ArgN<0, decltype(&foo)>>::value));
    ASSERT_TRUE((std::is_same<std::string, ArgN<1, decltype(&foo)>>::value));
    ASSERT_TRUE((std::is_same<int, ArgN<2, decltype(&foo)>>::value));
}

TEST(FuncTraits, test_ifTypesAreAsExpectedForLambdas) {
    auto l = [](int x, float y, double z) { return x + y + z; };
    ASSERT_TRUE((std::is_same<int, ArgN<0, decltype(l)>>::value));
    ASSERT_TRUE((std::is_same<float, ArgN<1, decltype(l)>>::value));
    ASSERT_TRUE((std::is_same<double, ArgN<2, decltype(l)>>::value));
}

TEST(FuncTraits, test_ifTypesAreAsExpectedForFuncObjects) {
    std::function<double(int, float, double)> l = [](int x, float y, double z) {
        return x + y + z;
    };

    ASSERT_TRUE((std::is_same<int, ArgN<0, decltype(l)>>::value));
    ASSERT_TRUE((std::is_same<float, ArgN<1, decltype(l)>>::value));
    ASSERT_TRUE((std::is_same<double, ArgN<2, decltype(l)>>::value));
}
