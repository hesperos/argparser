#include <gtest/gtest.h>

#include "common.hpp"

#include <argparser/argument.hpp>

using namespace argparser;

class TestableArgument : public Argument {
public:
    using Argument::getDescription;
    using Argument::getValue;
    using Argument::isAssigned;
    using Argument::isCumulative;
    using Argument::isRequired;
    using Argument::setValue;

    std::string getName() const override {
        return "stub";
    }

    bool isMatching(std::string_view) const override {
        return false;
    }

    std::string summarise() const override {
        return "arg_summary";
    }
};

class ArgumentTest : public ::testing::Test {
public:
};

TEST_F(ArgumentTest, test_ifSetsTheRequiredFlag) {
    TestableArgument ta{};
    ASSERT_FALSE(ta.isRequired());

    ta.required();
    ASSERT_TRUE(ta.isRequired());
}

TEST_F(ArgumentTest, test_ifSetsTheDescription) {
    TestableArgument ta{};
    ASSERT_EQ("[no description]", ta.getDescription());

    std::string desc = "this is some argument description";

    ta.desc(desc);
    ASSERT_EQ(desc, ta.getDescription());
}

TEST_F(ArgumentTest, test_ifItsPossibleToSetValue) {
    TestableArgument ta{};
    int i = 0;

    ASSERT_FALSE(ta.isAssigned());

    ta.set(i);
    ta.setValue("123");

    ASSERT_TRUE(ta.isAssigned());
    ASSERT_EQ("123", ta.getValue());
}

TEST_F(ArgumentTest, test_ifCumulativeIsFalseByDefault) {
    TestableArgument ta{};
    ASSERT_FALSE(ta.isCumulative());
}

TEST_F(ArgumentTest, test_ifPossibleToSetTheValidator) {
    TestableArgument ta{};
    int i = 0;

    ta.set(i);
    ta.validate([](int j) { return j >= 0; });

    ASSERT_NO_THROW(ta.setValue("123"));
    ASSERT_THROW(ta.setValue("-1"), ArgValidationEx);
}

TEST_F(ArgumentTest, test_ifValidationIsPerformedOnDefaultValue1) {
    TestableArgument ta{};
    int i = -100;

    ta.validate([](int j) { return j >= 0; });

    ASSERT_THROW(ta.set(i), ArgValidationEx);
}

TEST_F(ArgumentTest, test_ifValidationIsPerformedOnDefaultValue2) {
    TestableArgument ta{};
    int i = -100;

    ta.set(i);

    ASSERT_THROW(ta.validate([](int j) { return j >= 0; }), ArgValidationEx);
}

TEST_F(ArgumentTest, test_ifValidationWorksWithReferenceTypes) {
    TestableArgument ta{};
    int i = 0;

    ta.set(i);
    ta.validate([](const int& j) { return j >= 0; });

    ASSERT_NO_THROW(ta.setValue("123"));
    ASSERT_THROW(ta.setValue("-1"), ArgValidationEx);

    ASSERT_EQ("123", ta.getValue());
    ASSERT_EQ(123, i);
}

TEST_F(ArgumentTest, test_ifInitialValidationIsIgnoredWhenRequested1) {
    TestableArgument ta{};
    int i = -100;

    ASSERT_NO_THROW(ta.validate([](int j) { return j >= 0; }, false));
    ASSERT_NO_THROW(ta.set(i));
}

TEST_F(ArgumentTest, test_ifInitialValidationIsIgnoredWhenRequested2) {
    TestableArgument ta{};
    int i = -100;

    ASSERT_NO_THROW(ta.set(i));
    ASSERT_NO_THROW(ta.validate([](int j) { return j >= 0; }, false));
}

TEST_F(ArgumentTest, test_ifValidationWorksWithFileStream) {
    TestableArgument ta{};
    std::ifstream ifs;

    ta.set(ifs);
    ta.validate([](const std::ifstream& ifs) { return ifs.is_open(); }, false);

    const std::string pv = STRINGIFY(TEST_ROOT) "/testdata/testfile.txt";

    ta.setValue(pv);
    ASSERT_TRUE(ifs.is_open());
}
