#include <gtest/gtest.h>

#include <argparser/argparser.hpp>

using namespace argparser;

class ArgParserTest : public ::testing::Test {
public:
};

TEST_F(ArgParserTest, test_ifSettingDescriptionWorks) {
    ArgParser ap{};
    const std::string desc = "this is a description for argument parser";
    ap.desc(desc);
    ASSERT_EQ(desc, ap.getDescription());
}

TEST_F(ArgParserTest, test_ifProgNameRetrievalWorks) {
    ArgParser ap{};
    std::vector<std::string> positionals{};

    ap.addPositional("OTHER").required().set(positionals);

    const char* argv[] = {"program", "parameter", "other"};
    const auto argc = sizeof(argv) / sizeof(argv[0]);

    ap.parse(argc, argv);
    ASSERT_EQ(argv[0], ap.getProgName());
}

TEST_F(ArgParserTest, test_ifComplainsAboutExcessiveOptions) {
    ArgParser ap{};
    std::string p1;

    ap.addPositional("P1").required().set(p1);

    const char* argv[] = {"program", "parameter",
                          "this should trigger excessive arg exception"};
    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_THROW(ap.parse(argc, argv), ArgExcessiveEx);
}

TEST_F(ArgParserTest, test_ifComplainsIfOptionHasNoValue) {
    ArgParser ap{};
    std::string o;

    ap.addOption(shortName = "o").set(o);

    const char* argv[] = {
        "program",
        "-o",
    };

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_THROW(ap.parse(argc, argv), ArgWithoutValueEx);
}

TEST_F(ArgParserTest, test_ifMultiplePositionalsWorkCorrectly) {
    ArgParser ap{};
    std::string p1;
    std::string p2;
    int p3;
    double p4;

    ap.addPositional("P1").required().set(p1);

    ap.addPositional("P2").required().set(p2);

    ap.addPositional("P3").required().set(p3);

    ap.addPositional("P4").required().set(p4);

    const char* argv[] = {"program", "p1_parameter", "p2", "1234", "3.14"};
    const auto argc = sizeof(argv) / sizeof(argv[0]);

    ASSERT_NO_THROW(ap.parse(argc, argv));
    ASSERT_EQ(argv[1], p1);
    ASSERT_EQ(argv[2], p2);
    ASSERT_EQ(1234, p3);
    ASSERT_EQ(3.14, p4);
}

TEST_F(ArgParserTest, test_ifDefaultValueIsPreservedIfTheresNoAssignment) {
    ArgParser ap{};
    const std::string defaultValue = "positional default value";
    std::string p1 = defaultValue;

    ap.addPositional("P1").set(p1);

    const char* argv[] = {
        "program",
    };

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_NO_THROW(ap.parse(argc, argv));
    ASSERT_EQ(defaultValue, p1);
}

TEST_F(ArgParserTest, test_ifFlagParsingWorks) {
    ArgParser ap{};
    bool flag = false;
    std::string positional;
    int option = 0;

    ap.addPositional("P1").set(positional);

    ap.addOption(shortName = "o")
        .set(option)
        .required()
        .desc("this is a required option");

    ap.addFlag(shortName = "f", longName = "flag").set(flag);

    const char* argv[] = {"program", "-o", "1234", "positional", "--flag"};

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_NO_THROW(ap.parse(argc, argv));

    ASSERT_TRUE(flag);
    ASSERT_EQ("positional", positional);
    ASSERT_EQ(1234, option);
}

TEST_F(ArgParserTest, test_ifShortLongLookupWorks) {
    ArgParser ap{};
    std::vector<std::string> opts;

    ap.addOption(shortName = "o", longName = "option")
        .set(opts)
        .required()
        .desc("this is a required option");

    const char* argv[] = {"program", "-o", "first", "--option", "second"};

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_NO_THROW(ap.parse(argc, argv));

    ASSERT_EQ(2, opts.size());
    ASSERT_EQ("first", opts.at(0));
    ASSERT_EQ("second", opts.at(1));
}

TEST_F(ArgParserTest, test_ifReassignmentThrows) {
    ArgParser ap{};
    bool flag = false;
    std::string positional;
    int option = 0;

    ap.addPositional("P1").set(positional);

    ap.addOption(shortName = "o")
        .set(option)
        .required()
        .desc("this is a required option");

    ap.addFlag(shortName = "f", longName = "flag").set(flag);

    const char* argv[] = {"program",    "-o",     "1234",
                          "positional", "--flag", "--flag"};

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_THROW(ap.parse(argc, argv), ArgReassignmentEx);
}

TEST_F(ArgParserTest, test_ifThrowsOnUnknownArgs) {
    ArgParser ap{};
    bool flag = false;
    std::string positional;
    int option = 0;

    ap.addPositional("P1").set(positional);

    ap.addOption(shortName = "o")
        .set(option)
        .required()
        .desc("this is a required option");

    ap.addFlag(shortName = "f", longName = "flag").set(flag);

    const char* argv[] = {"program", "-o",        "1234",   "positional",
                          "--flag",  "--unknown", "--other"};

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_THROW(ap.parse(argc, argv), ArgUnknownEx);
}

TEST_F(ArgParserTest, test_ifThrowsOnMissingRequiredOption) {
    ArgParser ap{};
    int option = 0;
    bool flag = false;

    ap.addOption(shortName = "o", longName = "option")
        .set(option)
        .required()
        .desc("this is a required option");

    ap.addFlag(longName = "other").set(flag);

    const char* argv[] = {"program", "--other"};

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_THROW(ap.parse(argc, argv), ArgMissingEx);
}

TEST_F(ArgParserTest, test_ifThrowsOnMissingRequiredPositional) {
    ArgParser ap{};
    int pos = 0;
    bool flag = false;

    ap.addPositional("P").set(pos).required().desc(
        "this is a required positional");

    ap.addFlag(longName = "other").set(flag);

    const char* argv[] = {"program", "--other"};

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_THROW(ap.parse(argc, argv), ArgMissingEx);
}

TEST_F(ArgParserTest, test_ifReturnedProcessedArgumentCountIsCorrect) {
    ArgParser ap{};
    bool flag = false;
    std::string positional;
    int option = 0;

    ap.addPositional("P1").set(positional);

    ap.addOption(shortName = "o")
        .set(option)
        .required()
        .desc("this is a required option");

    ap.addFlag(shortName = "f", longName = "flag").set(flag);

    const char* argv[] = {
        "program", "-o", "1234", "positional", "--flag",
    };

    const auto argc = sizeof(argv) / sizeof(argv[0]);
    ASSERT_EQ(3, ap.parse(argc, argv));
}

TEST_F(ArgParserTest, test_ifValidatorIsAppliedToCorrectPositional) {
    ArgParser ap{};
    std::string positional;

    ap.addPositional("P1")
        .set(positional)
        .validate([](std::string s) { return s.size() > 0; }, false);

    const char* argv1[] = {
        "program",
        "positional",
    };
    const auto argc1 = sizeof(argv1) / sizeof(argv1[0]);
    ASSERT_NO_THROW(ap.parse(argc1, argv1));
}

TEST_F(ArgParserTest, test_ifValidatorIsAppliedToIncorrectPositional) {
    ArgParser ap{};
    std::string positional;

    ap.addPositional("P1")
        .set(positional)
        .validate([](std::string s) { return s.size() > 0; }, false);

    const char* argv2[] = {
        "program",
        "",
    };
    const auto argc2 = sizeof(argv2) / sizeof(argv2[0]);
    ASSERT_THROW(ap.parse(argc2, argv2), ArgValidationEx);
}

TEST_F(ArgParserTest, test_ifValidatorIsAppliedToArguments) {
    const std::unordered_map<std::string, bool> testData{
        {"0", true},  {"10", true}, {"-1", false},
        {"-0", true}, {"+0", true}, {"123", false},
    };

    for (const auto& [value, isCorrect] : testData) {
        ArgParser ap{};
        int i = 0;

        ap.addOption(shortName = "o")
            .validate([](int i) { return i >= 0 && i < 100; })
            .set(i);

        const char* argv2[] = {
            "program",
            "-o",
            value.c_str(),
        };
        const auto argc2 = sizeof(argv2) / sizeof(argv2[0]);

        if (isCorrect) {
            ASSERT_NO_THROW(ap.parse(argc2, argv2));
        } else {
            ASSERT_THROW(ap.parse(argc2, argv2), ArgValidationEx);
        }
    }
}
