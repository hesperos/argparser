#include <gtest/gtest.h>

#include <argparser/strutil.hpp>

class StrUtilTest : public ::testing::Test {
public:
};

TEST_F(StrUtilTest, test_ifConvertsToLowerCase) {
    const std::string given = "This is Some MIXED case string";
    const std::string expected = "this is some mixed case string";
    ASSERT_EQ(expected, argparser::lowercase(given));
}

TEST_F(StrUtilTest, test_ifBasenameExtractsLastPathComponent) {
    const std::string& p1 = "/this/is/some/path/with/slashes";
    const std::string& p2 = "\\This\\is\\a\\windows\\path";
    const std::string& p3 = "this/is\\a/path\\with/mixes\\slashes";
    const std::string& p4 = "no_slashes";
    const std::string& p5 = "/this/one/has/a/trailing/slash/";

    ASSERT_EQ("slashes", argparser::basename(p1));
    ASSERT_EQ("path", argparser::basename(p2));
    ASSERT_EQ("slashes", argparser::basename(p3));
    ASSERT_EQ(p4, argparser::basename(p4));
    ASSERT_EQ("", argparser::basename(p5));
}

TEST_F(StrUtilTest, test_ifIsOptionRejectsNonOptionArgs) {
    ASSERT_FALSE(argparser::isOption("blah"));
    ASSERT_FALSE(argparser::isOption("other"));
    ASSERT_FALSE(argparser::isOption("middle-hyphen"));
    ASSERT_FALSE(argparser::isOption("trailing-hyphens--"));
}

TEST_F(StrUtilTest, test_ifIsOptionRecognisesShortAndLongOpts) {
    ASSERT_TRUE(argparser::isOption("-s"));
    ASSERT_TRUE(argparser::isOption("--long"));
    ASSERT_TRUE(argparser::isOption("--long-middle-hyphen"));
    ASSERT_FALSE(argparser::isOption("-this-is-not-a-short-opt"));
}
