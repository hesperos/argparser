#include <argparser/argparser.hpp>

#include <iostream>

/*
 * Usage example:
 *
 * $ with_validation -o 1000 -a blah
 */
int main(int argc, const char* argv[]) {
    using namespace argparser;

    int option = 123;
    std::string other = "option's default value";
    bool helpFlag = false;

    ArgParser ap{};

    ap.desc("this is an example program with a simple set of CLI arguments")
        .version("123");

    ap.addOption(shortName = "o", longName = "option")
        .required()
        .desc("this is a mandatory option")
        .validate([](int v) { return v > 0 && v < 200; })
        .set(option);

    ap.addOption("a"_short, "another"_long)
        .desc("this option is not mandatory")
        .validate([](std::string v) { return v.size() < 10; })
        .set(other);

    ap.addFlag(shortName = "h", longName = "help")
        .desc("display options summary")
        .set(helpFlag)
        .terminate();

    try {
        ap.parse(argc, argv);
        if (helpFlag) {
            std::cout << ap << std::endl;
        }
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
