#include <argparser/argparser.hpp>

#include <iostream>

/*
 * Usage example:
 *
 * $ cumulatives -I /usr/bin -I /usr/local/bin README.md TODO.md -v -v -v
 *
 * Verbosity: 3
 * Include paths:
 * /usr/bin,/usr/local/bin,
 * Input files:
 * README.md,TODO.md,
 */
int main(int argc, const char* argv[]) {
    using namespace argparser;

    bool helpFlag = false;
    std::vector<std::string> incs;
    std::vector<bool> verbosity;
    std::vector<std::string> inputFiles;

    ArgParser ap{};
    ap.desc("This resembles a semantics of a gcc compiler");

    ap.addFlag(shortName = "h", longName = "help")
        .desc("display options summary")
        .set(helpFlag)
        .terminate();

    ap.addOption(shortName = "I", longName = "include")
        .desc("provide include search path")
        .set(incs);

    ap.addFlag(shortName = "v", longName = "verbosity")
        .desc("increase verbosity.  May be defined multiple times")
        .set(verbosity);

    ap.addPositional("INPUT_FILES")
        .desc("provide list of files")
        .required()
        .set(inputFiles);

    try {
        ap.parse(argc, argv);
        if (helpFlag) {
            std::cout << ap << std::endl;
            return EXIT_SUCCESS;
        }

        std::cout << "Verbosity: " << verbosity.size() << std::endl;

        std::cout << "Include paths: " << std::endl;
        std::copy(incs.begin(), incs.end(),
                  std::ostream_iterator<std::string>(std::cout, ","));
        std::cout << std::endl;

        std::cout << "Input files: " << std::endl;
        std::copy(inputFiles.begin(), inputFiles.end(),
                  std::ostream_iterator<std::string>(std::cout, ","));
        std::cout << std::endl;

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
