#include <argparser/argparser.hpp>

#include <iostream>

/*
 * Usage example:
 *
 * $ positionals 123 456 789
 * Positional arguments entered:
 * 123
 * 456
 * 789
 *
 */
int main(int argc, const char* argv[]) {
    using namespace argparser;

    std::vector<int> positionals;
    bool helpFlag = false;

    ArgParser ap{};
    ap.desc("demonstrates using one cumulative positional argument");

    ap.addFlag(shortName = "h", longName = "help")
        .desc("display options summary")
        .set(helpFlag)
        .terminate();

    ap.addPositional("NUMBERS")
        .desc("provide arbitrary amount of numbers")
        .set(positionals);

    try {
        ap.parse(argc, argv);
        if (helpFlag) {
            std::cout << ap << std::endl;
            return EXIT_SUCCESS;
        }

        std::cout << "Positional arguments entered: " << std::endl;
        std::copy(positionals.begin(), positionals.end(),
                  std::ostream_iterator<int>(std::cout, "\n"));
        std::cout << std::endl;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
