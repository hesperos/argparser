#include <argparser/argparser.hpp>

#include <filesystem>
#include <fstream>
#include <iostream>

/*
 * Usage example:
 *
 * $ positionals2 123 this_is_a_string 3.14
 * Positionals:
 * p1: 123, p2: this_is_a_string, p3: 3.14
 * option: default value
 */
int main(int argc, const char* argv[]) {
    using namespace argparser;
    int p1 = 0;
    std::string p2 = "default positional value";
    double p3 = 0;
    std::filesystem::path p4;
    std::ifstream ifs;

    bool helpFlag = false;
    std::string option = "default option value";

    ArgParser ap{};
    ap.desc("demonstrates using a mix of options and positionals");

    ap.addFlag(shortName = "h", longName = "help")
        .desc("display options summary")
        .set(helpFlag)
        .terminate();

    ap.addOption(shortName = "o", longName = "option")
        .desc("some arbitrary option")
        .set(option);

    ap.addPositional("NUMBER")
        .desc("provide a number positional argument - this one is mandatory")
        .required()
        .set(p1);

    ap.addPositional("STRING")
        .desc("provide a string positional argument")
        .set(p2);

    ap.addPositional("FLOAT")
        .desc("provide a float positional argument")
        .set(p3);

    ap.addPositional("PATH").desc("provide a path positional").set(p4);

    ap.addPositional("PATH2").set(ifs);

    try {
        ap.parse(argc, argv);
        if (helpFlag) {
            std::cout << ap << std::endl;
            return EXIT_SUCCESS;
        }

        std::cout << "Positionals: " << std::endl;
        std::cout << "p1: " << p1 << ", p2: " << p2 << ", p3: " << p3
                  << std::endl;
        std::cout << "option: " << option << std::endl;
        std::cout << "path: " << p4 << std::endl;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
