#ifndef POSITIONAL_H_
#define POSITIONAL_H_

#include <argparser/argument.hpp>

namespace argparser {
class Positional : public priv::Argument {
public:
    explicit Positional(std::string name);

    bool isMatching(std::string_view argName) const override;

    std::string getName() const override;

    std::string summarise() const override;

private:
    const std::string name;
};

} /* namespace argparser */

#endif /* POSITIONAL_H_ */
