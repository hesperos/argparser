#ifndef ARGPARSER_H_
#define ARGPARSER_H_

#include <argparser/argument.hpp>
#include <argparser/exceptions.hpp>
#include <argparser/option_name.hpp>

#include <memory>

namespace argparser {
class ArgParser {
public:
    ~ArgParser();

    explicit ArgParser();

    template <NameVariant... NamesT> Argument& addOption(NamesT&&... args) {
        static_assert(sizeof...(args) >= 1 && sizeof...(args) <= 2,
                      "incorrect number of option names");
        return addOptionWrapper(false, std::forward<NamesT>(args)...);
    }

    template <NameVariant... NamesT>
        requires((std::convertible_to<NamesT, std::string>) && ...)
    Argument& addFlag(NamesT&&... args) {
        static_assert(sizeof...(args) >= 1 && sizeof...(args) <= 2,
                      "incorrect number of flag names");
        return addOptionWrapper(true, std::forward<NamesT>(args)...);
    }

    Argument& addPositional(const std::string& posName);

    ArgParser& desc(const std::string& description);

    ArgParser& version(const std::string& version);

    std::size_t parse(std::size_t argc, const char* argv[]);

    std::string getProgName() const;

    std::string getDescription() const;

private:
    friend std::ostream& operator<<(std::ostream& os, const ArgParser& ap);

    class Impl;
    std::unique_ptr<Impl> pImpl;

    Argument& addOptionWrapper(bool, ShortName);
    Argument& addOptionWrapper(bool, LongName);
    Argument& addOptionWrapper(bool, ShortName, LongName);
    Argument& addOptionWrapper(bool, LongName, ShortName);
};

std::ostream& operator<<(std::ostream& os, const ArgParser& ap);
} /* namespace argparser */

#endif /* ARGPARSER_H_ */
