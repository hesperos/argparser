#ifndef _FUNC_TRAITS_HPP_
#define _FUNC_TRAITS_HPP_

#include <functional>
#include <tuple>
#include <type_traits>

namespace argparser::priv {

template <typename FuncT> struct ArgTraits;

// specialisation for function pointers
template <typename RetT, typename... ArgT> struct ArgTraits<RetT (*)(ArgT...)> {
    using type = std::tuple<ArgT...>;
};

// specialisation for function objects
template <typename RetT, typename... ArgT>
struct ArgTraits<std::function<RetT(ArgT...)>> {
    using type = std::tuple<ArgT...>;
};

// specialisation to retrieve the signature of the call operator
template <typename FuncT>
struct ArgTraits : public ArgTraits<decltype(&FuncT::operator())> {};

// specialisation for member function pointers
template <typename FuncT, typename RetT, typename... ArgT>
struct ArgTraits<RetT (FuncT::*)(ArgT...) const> {
    using type = std::tuple<ArgT...>;
};

template <std::size_t N, typename FuncT>
using ArgN = std::tuple_element<N, typename ArgTraits<FuncT>::type>::type;

} /* namespace argparser::priv */

#endif // !_FUNC_TRAITS_HPP_
