#ifndef OPTION_NAME_H_
#define OPTION_NAME_H_

#include <argparser/exceptions.hpp>

#include <concepts>
#include <string>
#include <string_view>

namespace argparser {
template <typename DerivedT> class OptionName {
public:
    virtual ~OptionName() = default;

    explicit OptionName(std::string name) :
        name{name} {
        if (name.empty()) {
            throw ArgDeclEx{name, "option/flag name is empty"};
        }

        if (name.find_first_of(' ') != std::string::npos) {
            throw ArgDeclEx{name, "option/flag name contains spaces"};
        }

        if (name.starts_with("-") || name.starts_with("--")) {
            throw ArgDeclEx{
                name, "option/flag name shouldn't contain hyphens as a prefix"};
        }
    }

    bool operator!=(const std::string& s) const {
        return !static_cast<const DerivedT*>(this)->operator==(s);
    }

    struct Arg {
        template <typename FromT> DerivedT operator=(FromT&& from) const {
            return DerivedT{std::forward<FromT>(from)};
        }
    };

protected:
    const std::string name;
};

class ShortName : public OptionName<ShortName> {
public:
    using OptionName<ShortName>::OptionName;
    bool operator==(const std::string& optName) const;
    operator std::string() const;
};

class LongName : public OptionName<LongName> {
public:
    using OptionName<LongName>::OptionName;
    bool operator==(const std::string& optName) const;
    operator std::string() const;
};

extern const ShortName::Arg shortName;
extern const LongName::Arg longName;

ShortName operator"" _short(const char* s, std::size_t);
LongName operator"" _long(const char* s, std::size_t);

} // namespace argparser

template <typename T>
concept NameVariant = std::same_as<T, argparser::ShortName> ||
                      std::same_as<T, argparser::LongName>;

#endif /* OPTION_NAME_H_ */
