#ifndef _VALIDATOR_HPP_
#define _VALIDATOR_HPP_

#include <any>
#include <functional>

namespace argparser::priv {

using Validator = std::function<bool(std::any)>;

} /* namespace argparser::priv */

#endif // !_VALIDATOR_HPP_
