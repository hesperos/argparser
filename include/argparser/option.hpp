#ifndef OPTION_H_
#define OPTION_H_

#include <argparser/argument.hpp>
#include <argparser/option_name.hpp>

#include <optional>
#include <string>
#include <string_view>

namespace argparser {
class Option : public priv::Argument {
public:
    Option(ShortName sv);

    Option(LongName lv);

    Option(ShortName sv, LongName lv);

    bool isMatching(std::string_view argName) const override;

    std::string getName() const override;

    std::string summarise() const override;

    virtual bool isNullary() const noexcept;

private:
    const std::optional<ShortName> shortVariant;
    const std::optional<LongName> longVariant;
};

} /* namespace argparser */

#endif /* OPTION_H_ */
