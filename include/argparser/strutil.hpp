#ifndef STRUTIL_H_
#define STRUTIL_H_

#include <string>
#include <string_view>

namespace argparser {
std::string lowercase(std::string s);

std::string basename(const std::string& p);

bool isOption(std::string_view sv);

} // namespace argparser
#endif /* STRUTIL_H_ */
