#ifndef VALUE_WRAPPER_H_
#define VALUE_WRAPPER_H_

#include <argparser/exceptions.hpp>
#include <argparser/strutil.hpp>
#include <argparser/validator.hpp>

#include <algorithm>
#include <cerrno>
#include <concepts>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <limits>
#include <sstream>
#include <string_view>
#include <type_traits>
#include <vector>

namespace argparser {
class ValueWrapper {
public:
    explicit ValueWrapper(priv::Validator val) :
        val{std::move(val)} {
    }

    virtual ~ValueWrapper() = default;

    virtual void setValue(std::string_view s) = 0;

    virtual std::string getValue() const = 0;

    virtual bool isCumulative() const = 0;

    virtual bool isAssigned() const = 0;

    virtual void validate() const = 0;

    virtual void setValidator(priv::Validator val) {
        this->val = std::move(val);
    }

protected:
    priv::Validator val;
};

template <typename ValueT> class Converter;

template <std::signed_integral ValueT> class Converter<ValueT> {
public:
    static ValueT convert(std::string_view s) {
        errno = 0;
        const auto& value = std::strtoll(s.data(), nullptr, 0);
        if (0 == value && errno != 0) {
            throw ArgConversionEx{std::string{s},
                                  "is not convertable to signed integral type"};
        }

        if (value > std::numeric_limits<ValueT>::max() ||
            (value < std::numeric_limits<ValueT>::min())) {
            throw ArgConversionEx{std::string{s}, "overflows"};
        }

        return value;
    }
};

template <std::unsigned_integral ValueT> class Converter<ValueT> {
public:
    static ValueT convert(std::string_view s) {
        errno = 0;
        const auto& value = std::strtoull(s.data(), nullptr, 0);
        if (0 == value && errno != 0) {
            throw ArgConversionEx{
                std::string{s}, "is not convertable to unsigned integral type"};
        }

        if (value > std::numeric_limits<ValueT>::max()) {
            throw ArgConversionEx{std::string{s}, "overflows"};
        }

        return value;
    }
};

template <std::floating_point FloatingT> class Converter<FloatingT> {
public:
    static FloatingT convert(std::string_view s) {
        errno = 0;
        const auto& value = std::strtod(s.data(), nullptr);
        if (0 == value && errno != 0) {
            throw ArgConversionEx{
                std::string{s}, "is not convertable to double precision type"};
        }

        if (value > std::numeric_limits<FloatingT>::max() ||
            (value < std::numeric_limits<FloatingT>::min())) {
            throw ArgConversionEx{std::string{s}, "overflows"};
        }

        return value;
    }
};

template <typename StreamT> struct StreamTag {};

template <typename ValueT> class Converter<StreamTag<ValueT>> {
public:
    static std::pair<ValueT, std::filesystem::path>
    convert(std::string_view s) {
        const std::string str{s};
        ValueT stream{str};
        if (!stream.is_open()) {
            throw ArgConversionEx{str, "unable to open path"};
        }
        return std::make_pair(std::move(stream), std::filesystem::path{str});
    }
};

template <typename AssignableT>
    requires std::constructible_from<AssignableT, std::string>
class Converter<AssignableT> {
public:
    static AssignableT convert(std::string_view s) {
        return std::string{s};
    }
};

template <> class Converter<bool> {
public:
    static bool convert(std::string_view s) {
        bool value = false;
        const auto& inStr = std::string{s};
        std::istringstream ss{lowercase(inStr)};
        ss >> value;
        if (ss.fail()) {
            ss.clear();
            ss >> std::boolalpha >> value;
        }
        if (ss.fail()) {
            throw ArgConversionEx{inStr, "is not convertible to boolean"};
        }
        return value;
    }
};

template <typename ValueT> class ValueWrapperForTBase : public ValueWrapper {
public:
    explicit ValueWrapperForTBase(priv::Validator val) :
        ValueWrapper(std::move(val)) {
    }

protected:
    virtual void doValidate(std::string_view s, const ValueT& candidate) const {
        if (this->val && !this->val(std::cref(candidate))) {
            throw ArgValidationEx{std::string(s)};
        }
    }
};

template <typename ValueT>
class ValueWrapperForT : public ValueWrapperForTBase<ValueT> {
public:
    ValueWrapperForT(ValueT& v, priv::Validator val) :
        ValueWrapperForTBase<ValueT>(std::move(val)),
        v{v},
        isArgAlreadyAssigned{false} {
    }

    bool isAssigned() const override {
        return isArgAlreadyAssigned;
    }

    bool isCumulative() const override {
        return false;
    }

    void setValue(std::string_view s) override {
        auto candidate = Converter<ValueT>::convert(s);
        this->doValidate(s, candidate);
        this->v = std::move(candidate);
        this->isArgAlreadyAssigned = true;
    }

    std::string getValue() const override {
        std::stringstream ss;
        ss << v;
        return ss.str();
    }

    void validate() const override {
        this->doValidate("", this->v);
    }

protected:
    ValueT& v;
    bool isArgAlreadyAssigned;
};

template <typename ValueT>
class ValueWrapperForT<std::vector<ValueT>>
    : public ValueWrapperForTBase<ValueT> {
public:
    ValueWrapperForT(std::vector<ValueT>& vec, priv::Validator val) :
        ValueWrapperForTBase<ValueT>(std::move(val)),
        vec{vec} {
    }

    bool isAssigned() const override {
        return !this->vec.empty();
    }

    bool isCumulative() const override {
        return true;
    }

    void setValue(std::string_view s) override {
        auto candidate = Converter<ValueT>::convert(s);
        this->doValidate(s, candidate);
        this->vec.emplace_back(std::move(candidate));
    }

    std::string getValue() const override {
        std::stringstream ss;
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<ValueT>(ss, ","));
        auto serialised = ss.str();
        auto lastComma = serialised.find_last_of(',');
        return serialised.substr(0, lastComma);
    }

    void validate() const override {
        for (const auto& v : vec) {
            this->doValidate("", v);
        }
    }

protected:
    std::vector<ValueT>& vec;
};

template <typename ValueT>
    requires std::same_as<std::ifstream, ValueT> ||
             std::same_as<std::ofstream, ValueT>
class ValueWrapperForT<ValueT> : public ValueWrapperForTBase<ValueT> {
public:
    ValueWrapperForT(ValueT& fs, priv::Validator val) :
        ValueWrapperForTBase<ValueT>(std::move(val)),
        fs{fs} {
    }

    bool isAssigned() const override {
        return !p.empty();
    }

    bool isCumulative() const override {
        return false;
    }

    void setValue(std::string_view s) override {
        auto [fs, p] = Converter<StreamTag<ValueT>>::convert(s);
        this->doValidate(s, fs);
        this->p = p;
        this->fs = std::move(fs);
    }

    std::string getValue() const override {
        return p.string();
    }

    void validate() const override {
        this->doValidate("", fs);
    }

protected:
    ValueT& fs;
    std::filesystem::path p;
};

} /* namespace argparser */

#endif /* VALUE_WRAPPER_H_ */
