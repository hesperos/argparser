#ifndef FLAG_H_
#define FLAG_H_

#include <argparser/option.hpp>

namespace argparser {
class Flag : public Option {
public:
    using Option::Option;

    void setValue(std::string_view s) override;

    bool isNullary() const noexcept override;
};

} /* namespace argparser */

#endif /* FLAG_H_ */
