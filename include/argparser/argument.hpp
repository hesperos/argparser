#ifndef ARGUMENT_H_
#define ARGUMENT_H_

#include <argparser/func_traits.hpp>
#include <argparser/validator.hpp>
#include <argparser/value_wrapper.hpp>

#include <memory>
#include <optional>
#include <string_view>

namespace argparser {

class Argument {
public:
    virtual ~Argument() = default;

    Argument& required();

    Argument& desc(const std::string& description);

    Argument& terminate();

    template <typename FuncT>
    Argument& validate(FuncT f, bool performInitialValidation = true) {
        auto val = [f = std::move(f)](std::any a) -> bool {
            using Arg0 = priv::ArgN<0, FuncT>;
            using Arg0Ref = std::reference_wrapper<
                const typename std::remove_cvref<Arg0>::type>;
            return f(std::any_cast<Arg0Ref>(a));
        };

        if (valueRef) {
            valueRef->setValidator(val);
            if (performInitialValidation) {
                valueRef->validate();
            }
        } else {
            this->validator = std::move(val);
            this->performInitialValidation = performInitialValidation;
        }

        return *this;
    }

    template <typename ValueT> Argument& set(ValueT&& value) {
        valueRef = std::make_unique<
            ValueWrapperForT<typename std::remove_reference<ValueT>::type>>(
            std::forward<ValueT>(value), std::move(validator));
        if (this->performInitialValidation) {
            valueRef->validate();
        }
        return *this;
    }

protected:
    bool isRequired() const noexcept;

    std::string getDescription() const;

    virtual void setValue(std::string_view s);

    std::optional<std::string> getValue() const;

    bool isAssigned() const noexcept;

    bool isCumulative() const noexcept;

    bool isTerminating() const noexcept;

    virtual std::string getName() const = 0;

    virtual bool isMatching(std::string_view argName) const = 0;

    virtual std::string summarise() const = 0;

private:
    friend std::ostream& operator<<(std::ostream& os, const Argument& arg);

    bool isArgumentTerminating{false};
    bool isArgumentRequired{false};
    std::string description;
    std::unique_ptr<ValueWrapper> valueRef;
    priv::Validator validator;
    bool performInitialValidation{true};
};

std::ostream& operator<<(std::ostream& os, const Argument& arg);

namespace priv {
class Argument : public ::argparser::Argument {
public:
    using ::argparser::Argument::getDescription;
    using ::argparser::Argument::getName;
    using ::argparser::Argument::getValue;
    using ::argparser::Argument::isAssigned;
    using ::argparser::Argument::isCumulative;
    using ::argparser::Argument::isMatching;
    using ::argparser::Argument::isRequired;
    using ::argparser::Argument::isTerminating;
    using ::argparser::Argument::setValue;
    using ::argparser::Argument::summarise;
};
} /* namespace priv */
} /* namespace argparser */

#endif /* ARGUMENT_H_ */
