#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <stdexcept>
#include <string>

namespace argparser {
class ArgConversionEx : public std::invalid_argument {
public:
    explicit ArgConversionEx(std::string value, std::string msg);
};

class ArgValidationEx : public std::invalid_argument {
public:
    explicit ArgValidationEx(std::string value);
};

class ArgExcessiveEx : public std::invalid_argument {
public:
    explicit ArgExcessiveEx(std::string argName);
};

class ArgDeclEx : public std::invalid_argument {
public:
    explicit ArgDeclEx(std::string argName, std::string msg);
};

class ArgDeclDuplicateEx : public std::logic_error {
public:
    explicit ArgDeclDuplicateEx(std::string argName);
};

class ArgWithoutValueEx : public std::invalid_argument {
public:
    explicit ArgWithoutValueEx(std::string argName);
};

class ArgMissingEx : public std::runtime_error {
public:
    explicit ArgMissingEx(std::string argName);
};

class ArgUnknownEx : public std::invalid_argument {
public:
    explicit ArgUnknownEx(std::string argName);
};

class ArgReassignmentEx : public std::runtime_error {
public:
    explicit ArgReassignmentEx(std::string argName);
};
} /* namespace argparser */

#endif /* EXCEPTIONS_H_ */
