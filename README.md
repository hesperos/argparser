# ArgParser - CLI command line parser for C++

This is yet another implementation of a CLI argument parser for C++.
Implemented as a lightweight alternative, if you don't want to reach out for
Boost's [`program_options`](https://www.boost.org/doc/libs/1_63_0/doc/html/program_options.html) and much more convenient in usage than bare `getopt`.

## Requirements

Relies on C++20 features (mainly concepts).  Toolchain supporting C++20 is
needed.

## Usage

First declare the parser:

	argparser::ArgParser ap{};
	
Add options, flags and positional arguments to it and bind them to variables:

```C++
const int defaultValue = 123;

int option = defaultValue;
bool flag = false;
std::string positional = "default value for positional";

ap.addOption("-o", "--option")
	.set(value)
	.desc("this is a description for the option")
	.required()
	;
	
ap.addFlag("-f", "--flag")
	.set(flag)
	;

ap.addPositional("P")
	.set(positional)
	.desc("this is a description for positional argument")
	;
```

Now, parse your options!

```
ap.parse(argc, argv);
```

The values will be assigned to bound variables.

### Options, Flags, Positionals... what's the difference?

It's simple, an option has a value i.e:

	myprogram --option value

A flag has no value that can be assigned to it.  Flags are meant to be bound to `bool` variables:

	myprogram --flag
	
A positional is a short for positional argument.  An argument that is neither a flag nor an option:

	myprogram file.txt
	
Each of these argument types can be bound to variables of different types. `ArgParser` currently supports the following types:

- `std::string`,
-  `int`, `unsigned`, `uint8`... any signed/unsigned integral type,
- `double`,
- `bool`

#### Cumulative types

Aside from the basic types mentioned above, you can have cumulative options, flags or positional arguments and bind them to `std::vector` of basic types.  I.e.:

```
std::vector<int> numbers;
std::vector<std::string> files;

ap.addOption("-n", "--number")
	.set(numbers)
	.required()
	.desc("a set of numbers")
	;
		
ap.addPositional("FILES")
	.set(files)
	.required()
	.desc("a set of input files")
	;
```
#### Validation

Values assign to arguments can be validated after conversion.  This is done
through validation functions.  To attach a validation function to an argument
use `.validate()`.  For example:

```c++
int i = 123;
try {
    ap.addPositional("INTEGER")
        .set(i)
        .validate([](int a){ return a > 0; });

    ap.parse(argc, argv);
}
catch (const ArgValidationEx& ex) {
}
```

**Validation function can't be a polymorphic lambda**. First argument has to
have a type that's convertible from the bound type.

Validation is performed on the initial value as well - this is done by default
but this behaviour can be disabled.

```c++
int i = 123;
int j = -100;

try {
    ap.addOption("-i")
        .set(i)
        // This will throw as i = 123 which fails the validation function.
        .validate([](int a){ return a < 100; }); 

    ap.addOption("-j")
        .set(j)
        // This will not throw - initial validation is explicitly disabled (last argument)!
        .validate([](int a){ return a > 0; }, false);
    ...

    ap.parse(argc, argv);
}
catch (const ArgValidationEx& ex) {
}


#### Argument attributes

You can assign the following attributes to each argument:

- `required()` - indicates that the argument is mandatory,
- `desc(std::string)` - assigns a description to an argument,
- `terminate()` - indicates that the parser should abort parsing once it finds an argument with this attribute
- `set(value_type)` - binds a variable to the argument.  This one is necessary and it is a programming error if the argument is not bound to any variable.


### Examples

To find out more about the usage, have a look on the provided [examples](/examples).

## Building and installation

`ArgParser` builds as a shared library.  Link against it to use it in your
project. Wrap file is provided as an example.  Add it to your subprojects
directory and use like any other meson subproject:

```
argparser = subproject('hesperos-libargparser')
argparser_dep = argparser.get_variable('libargparser_dep')

...

executable('myprogram',
  'myprogram.cpp',
  dependencies : argparser_dep,
  install : true)
```

## `ArgParser` on `twdev.blog`

You can read more about `ArgParser` on my [blog](https://twdev.blog/2021/12/argparser/).

---
(c) 2021 dagon666
