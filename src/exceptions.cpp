#include <argparser/exceptions.hpp>

namespace argparser {
ArgConversionEx::ArgConversionEx(std::string value, std::string msg) :
    std::invalid_argument("'" + value + "' " + msg) {
}

ArgValidationEx::ArgValidationEx(std::string value) :
    std::invalid_argument("Invalid value: '" + value + "'") {
}

ArgExcessiveEx::ArgExcessiveEx(std::string argName) :
    std::invalid_argument("Argument is excessive: '" + argName + "'") {
}

ArgDeclEx::ArgDeclEx(std::string argName, std::string msg) :
    std::invalid_argument("'" + argName + "' " + msg) {
}

ArgDeclDuplicateEx::ArgDeclDuplicateEx(std::string argName) :
    std::logic_error("Argument: '" + argName + "' has been already declared") {
}

ArgWithoutValueEx::ArgWithoutValueEx(std::string argName) :
    std::invalid_argument("Required value for argument: '" + argName +
                          "' is missing") {
}

ArgMissingEx::ArgMissingEx(std::string argName) :
    std::runtime_error("Required argument: '" + argName + "' is missing") {
}

ArgUnknownEx::ArgUnknownEx(std::string argName) :
    std::invalid_argument("Argument '" + argName + "' is not known") {
}

ArgReassignmentEx::ArgReassignmentEx(std::string argName) :
    std::runtime_error("Argument '" + argName +
                       "' has already value assigned") {
}
} /* namespace argparser */
