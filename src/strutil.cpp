#include <argparser/strutil.hpp>

#include <algorithm>
#include <string>

namespace argparser {
std::string lowercase(std::string s) {
    std::string lowercase;
    lowercase.resize(s.size());

    std::transform(s.begin(), s.end(), lowercase.begin(),
                   [](auto c) { return std::tolower(c); });

    return lowercase;
}

std::string basename(const std::string& p) {
    const auto pos = p.find_last_of("/\\");
    return p.substr(pos + 1);
}

bool isOption(std::string_view sv) {
    if (sv.size() < 2) {
        return false;
    }

    return (sv.size() == 2 && sv[0] == '-') ||
           (sv.size() > 2 && sv.substr(0, 2) == "--");
}

} // namespace argparser
