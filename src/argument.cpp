#include <argparser/argument.hpp>

namespace argparser {
Argument& Argument::required() {
    isArgumentRequired = true;
    return *this;
}

Argument& Argument::desc(const std::string& description) {
    this->description = description;
    return *this;
}

Argument& Argument::terminate() {
    this->isArgumentTerminating = true;
    return *this;
}

bool Argument::isRequired() const noexcept {
    return isArgumentRequired;
}

bool Argument::isTerminating() const noexcept {
    return isArgumentTerminating;
}

std::string Argument::getDescription() const {
    return description.size() > 0 ? description : "[no description]";
}

void Argument::setValue(std::string_view s) {
    if (!valueRef) {
        return;
    }

    valueRef->setValue(s);
}

std::optional<std::string> Argument::getValue() const {
    if (valueRef) {
        return valueRef->getValue();
    }
    return {};
}

bool Argument::isAssigned() const noexcept {
    return valueRef && valueRef->isAssigned();
}

bool Argument::isCumulative() const noexcept {
    return valueRef && valueRef->isCumulative();
}

std::ostream& operator<<(std::ostream& os, const Argument& arg) {
    os << "\t" << arg.getName();
    if (auto defaultValue = arg.getValue(); defaultValue.has_value()) {
        os << " (=" << *defaultValue << ") ";
    }
    os << "\t- " << arg.getDescription();
    return os;
}
} /* namespace argparser */
