#include <argparser/option_name.hpp>

namespace argparser {
const ShortName::Arg shortName;
const LongName::Arg longName;

bool ShortName::operator==(const std::string& optName) const {
    return optName.substr(1) == name;
}

ShortName::operator std::string() const {
    return "-" + name;
}

bool LongName::operator==(const std::string& optName) const {
    return optName.substr(2) == name;
}

LongName::operator std::string() const {
    return "--" + name;
}

ShortName operator"" _short(const char* s, std::size_t) {
    return ShortName{std::string(s)};
}

LongName operator"" _long(const char* s, std::size_t) {
    return LongName{std::string(s)};
}

} // namespace argparser
