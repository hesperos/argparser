#include <argparser/argparser.hpp>
#include <argparser/flag.hpp>
#include <argparser/option.hpp>
#include <argparser/positional.hpp>

#include <list>
#include <unordered_map>
#include <vector>

namespace argparser {
class ArgParser::Impl {
public:
    using Options = std::list<std::unique_ptr<Option>>;
    using OptMap = std::unordered_map<std::string, Options::iterator>;
    using PosMap = std::vector<std::unique_ptr<Positional>>;

    Option& getOpt(std::string name) const {
        if (auto it = optMap.find(name);
            it != optMap.end() && (*it->second)->isMatching(name)) {
            return *(*it->second);
        }
        throw ArgUnknownEx{name};
    }

    void isArgDefined(const std::string& name) {
        if (auto it = optMap.find(name); it != optMap.end()) {
            throw ArgDeclDuplicateEx{(*it->second)->getName()};
        }
    }

    void mapArgument(const std::string& name, Options::iterator it) {
        optMap.emplace(std::piecewise_construct, std::forward_as_tuple(name),
                       std::forward_as_tuple(it));
    }

    Argument& addOption(ShortName sv, bool isFlag) {
        isArgDefined(static_cast<std::string>(sv));
        auto it =
            opts.insert(opts.end(), isFlag ? std::make_unique<Flag>(sv)
                                           : std::make_unique<Option>(sv));
        mapArgument(static_cast<std::string>(sv), it);
        return *(*it);
    }

    Argument& addOption(LongName lv, bool isFlag) {
        isArgDefined(static_cast<std::string>(lv));
        auto it =
            opts.insert(opts.end(), isFlag ? std::make_unique<Flag>(lv)
                                           : std::make_unique<Option>(lv));
        mapArgument(static_cast<std::string>(lv), it);
        return *(*it);
    }

    Argument& addOption(ShortName sv, LongName lv, bool isFlag) {
        isArgDefined(static_cast<std::string>(sv));
        isArgDefined(static_cast<std::string>(lv));

        auto it =
            opts.insert(opts.end(), isFlag ? std::make_unique<Flag>(sv, lv)
                                           : std::make_unique<Option>(sv, lv));

        mapArgument(static_cast<std::string>(sv), it);
        mapArgument(static_cast<std::string>(lv), it);
        return *(*it);
    }

    Argument& addPositional(const std::string& posName) {
        if (std::find_if(positionals.begin(), positionals.end(),
                         [&posName](const auto& pos) {
                             return pos->getName() == posName;
                         }) != positionals.end()) {
            throw ArgDeclDuplicateEx{posName};
        }

        positionals.push_back(std::make_unique<Positional>(posName));
        return *positionals.back();
    }

    std::size_t parseArgs(std::size_t argc, const char* argv[]) {
        auto posIt = positionals.begin();
        std::size_t nArgs = 0;

        for (std::size_t i = 1; i < argc; ++i) {
            if (isOption(argv[i])) {
                Option& opt = getOpt(argv[i]);

                // check if argument has associated value
                if (!opt.isNullary() && i == (argc - 1)) {
                    throw ArgWithoutValueEx{argv[i]};
                }

                if (opt.isAssigned() && !opt.isCumulative()) {
                    throw ArgReassignmentEx{argv[i]};
                }

                if (opt.isNullary()) {
                    opt.setValue("");
                } else {
                    opt.setValue(argv[++i]);
                }

                nArgs++;
                if (opt.isTerminating()) {
                    isTerminatedByArgument = true;
                    break;
                }

            } else {
                // must be a positional
                if (posIt == positionals.end()) {
                    throw ArgExcessiveEx(argv[i]);
                }

                if ((*posIt)->isAssigned() && !(*posIt)->isCumulative()) {
                    throw ArgReassignmentEx{(*posIt)->getName()};
                }

                (*posIt)->setValue(argv[i]);

                // If positional is a cumulative argument, then maintain
                // current position.  This positional will effectively
                // consume the remainder of all arguments.
                if (!(*posIt)->isCumulative()) {
                    ++posIt;
                }

                nArgs++;
            }
        }

        return nArgs;
    }

    std::size_t parse(std::size_t argc, const char* argv[]) {
        progName = basename(argv[0]);
        const auto rv = parseArgs(argc, argv);

        // Don't verify if we've got complete set of required arguments if
        // one of them has forcefully terminated option parsing (this may
        // happen i.e. if there's a terminating definition of a '--help'
        // option)
        if (!isTerminatedByArgument) {
            checkRequiredArgs(opts);
            checkRequiredArgs(positionals);
        }
        return rv;
    }

    template <typename T> void checkRequiredArgs(T&& opts) {
        for (const auto& opt : std::forward<T>(opts)) {
            if (opt->isRequired() && !opt->isAssigned()) {
                throw ArgMissingEx{opt->getName()};
            }
        }
    }

    std::string getProgName() const {
        return progName;
    }

    std::string getDescription() const {
        return description.size() > 0 ? description : "[no description]";
    }

    std::string getVersion() const {
        return version;
    }

    std::string getArgSummary() const {
        std::stringstream summary;

        for (const auto& opt : opts) {
            summary << opt->summarise() << " ";
        }

        for (const auto& pos : positionals) {
            summary << pos->summarise() << " ";
        }

        return summary.str();
    }

    void setDescription(const std::string& desc) {
        description = desc;
    }

    void setVersion(const std::string& version) {
        this->version = version;
    }

    const Options& getOpts() const {
        return opts;
    }

    const PosMap& getPositionals() const {
        return positionals;
    }

private:
    std::string description;
    std::string progName;
    std::string version;
    bool isTerminatedByArgument{false};

    // arguments in long and short form
    Options opts;
    OptMap optMap;
    PosMap positionals;
};

ArgParser::~ArgParser() {
    // non-inline dtor is required to make pImpl work
}

ArgParser::ArgParser() :
    pImpl(std::make_unique<ArgParser::Impl>()) {
}

Argument& ArgParser::addOptionWrapper(bool isFlag, ShortName s) {
    return pImpl->addOption(s, isFlag);
}

Argument& ArgParser::addOptionWrapper(bool isFlag, LongName l) {
    return pImpl->addOption(l, isFlag);
}

Argument& ArgParser::addOptionWrapper(bool isFlag, ShortName s, LongName l) {
    return pImpl->addOption(s, l, isFlag);
}

Argument& ArgParser::addOptionWrapper(bool isFlag, LongName l, ShortName s) {
    return pImpl->addOption(s, l, isFlag);
}

Argument& ArgParser::addPositional(const std::string& posName) {
    return pImpl->addPositional(posName);
}

ArgParser& ArgParser::desc(const std::string& description) {
    pImpl->setDescription(description);
    return *this;
}

ArgParser& ArgParser::version(const std::string& version) {
    pImpl->setVersion(version);
    return *this;
}

std::size_t ArgParser::parse(std::size_t argc, const char* argv[]) {
    return pImpl->parse(argc, argv);
}

std::string ArgParser::getProgName() const {
    return pImpl->getProgName();
}

std::string ArgParser::getDescription() const {
    return pImpl->getDescription();
}

std::ostream& operator<<(std::ostream& os, const ArgParser& ap) {
    os << "Usage: " << ap.getProgName() << " " << ap.pImpl->getArgSummary()
       << std::endl;

    if (const auto& versionStr = ap.pImpl->getVersion(); !versionStr.empty()) {
        os << "Version: " << versionStr << std::endl;
    }
    os << std::endl;

    os << ap.getDescription() << std::endl;
    os << std::endl;

    os << "Positional arguments: " << std::endl;
    for (const auto& pos : ap.pImpl->getPositionals()) {
        os << (*pos) << std::endl;
    }
    os << std::endl;

    os << "Flags and options: " << std::endl;
    for (const auto& opt : ap.pImpl->getOpts()) {
        os << (*opt) << std::endl;
    }

    return os;
}
} /* namespace argparser */
