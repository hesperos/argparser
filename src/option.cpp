#include <argparser/option.hpp>

namespace argparser {
Option::Option(ShortName sv) :
    shortVariant{sv} {
}

Option::Option(LongName lv) :
    longVariant{lv} {
}

Option::Option(ShortName sv, LongName lv) :
    shortVariant{sv},
    longVariant{lv} {
}

bool Option::isMatching(const std::string_view argName) const {
    return (shortVariant.has_value() &&
            (*shortVariant) == std::string(argName)) ||
           (longVariant.has_value() && (*longVariant) == std::string(argName));
}

std::string Option::getName() const {
    if (shortVariant.has_value() && longVariant.has_value()) {
        return static_cast<std::string>(*shortVariant) + "|" +
               static_cast<std::string>(*longVariant);
    }

    if (shortVariant.has_value()) {
        return *shortVariant;
    }

    return *longVariant;
}

std::string Option::summarise() const {
    std::stringstream ss;
    const auto& braces =
        isRequired() ? std::make_pair('<', '>') : std::make_pair('[', ']');

    ss << braces.first << getName();
    if (!isNullary()) {
        ss << " value";
    }

    ss << braces.second;
    if (isCumulative()) {
        ss << '+';
    }

    return ss.str();
}

bool Option::isNullary() const noexcept {
    return false;
}
} /* namespace argparser */
