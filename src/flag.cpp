#include <argparser/flag.hpp>

namespace argparser {
bool Flag::isNullary() const noexcept {
    return true;
}

void Flag::setValue(std::string_view) {
    priv::Argument::setValue("1");
}
} /* namespace argparser */
