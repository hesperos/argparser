#include <argparser/positional.hpp>

namespace argparser {
Positional::Positional(std::string name) :
    name{name} {
    if (name.starts_with('-') || name.starts_with("--")) {
        throw ArgDeclEx{getName(),
                        "positional argument's name has a hyphen prefix"};
    }

    if (name.empty()) {
        throw ArgDeclEx{getName(), "positional argument's name is empty"};
    }

    if (name.find_first_of(' ') != std::string::npos) {
        throw ArgDeclEx{getName(),
                        "positional argument's name contains spaces"};
    }
}

bool Positional::isMatching(std::string_view) const {
    // Positionals are not named explicitly hence won't ever match against
    // any pattern.
    return false;
}

std::string Positional::summarise() const {
    std::stringstream ss;
    const auto& braces =
        isRequired() ? std::make_pair('<', '>') : std::make_pair('[', ']');

    ss << braces.first << getName() << braces.second;
    if (isCumulative()) {
        ss << '+';
    }
    return ss.str();
}

std::string Positional::getName() const {
    return name;
}

} /* namespace argparser */
