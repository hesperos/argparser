# Contributing to `ArgParser`

`ArgParser` is open to pull requests.

## How to contribute?

Contribute the same way as to any other gitlab project:
- Fork the repo,
- Add changes on a branch
- Raise a pull requests

Commits should be written in imperative mode and contain at least a minimal description.  Unit tests are always welcomed.
